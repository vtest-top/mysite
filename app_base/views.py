from django.shortcuts import render, HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate

from app_base.forms.account import LoginForm

import json

# Create your views here.


def login(request):
    if request.POST:
        result = {
            'status': False,
            'message': None,
            'data': None
        }
        # form = LoginForm(request=request, data=request.POST)
        #
        # if form.is_valid():
        #     username = request.POST.get('username')
        #     password = request.POST.get('password')
        #     user = User.objects.filter(username=username, password=password)
        #     print(user)
        #     if user:
        #         print(1)
        #     else:
        #         result['message'] = '用户名或密码错误！'
        # print(result)
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        print(username, password)
        user = authenticate(username=username, password=password)
        print(user)
        if user:
            result['status'] = True
            result['message'] = '登录成功！'
        else:
            result['message'] = '用户名或密码错误！'
        print(result)
        return HttpResponse(json.dumps(result))
    else:
        return render(request, 'account/login.html')


def home(request):
    return render(request, 'index.html')

