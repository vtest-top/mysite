# -*- coding: utf-8 -*-
# @Time    : 2019/3/30 7:04
# @Author  : fuqiang
# @File    : base.py
# @Desc    :


class BaseForm:

    def __init__(self, request, *args, **kwargs):
        self.request = request
        super(BaseForm, self).__init__(*args, **kwargs)
