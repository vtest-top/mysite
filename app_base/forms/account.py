# -*- coding: utf-8 -*-
# @Time    : 2019/3/30 7:02
# @Author  : fuqiang
# @File    : account.py
# @Desc    : 用户角色form

from django.contrib.auth.models import User
from app_base.forms.base import BaseForm
from django.core.exceptions import ValidationError
from django import forms as django_forms
from django.forms import fields as django_fields
from django.forms import widgets as django_widgets


class LoginForm(BaseForm, django_forms.Form):
    username = django_fields.CharField(
        min_length=6,
        max_length=16,
        error_messages={
            'required': '用户名不能为空',
            'min_length': '用户名长度不能小于6个字符',
            'max_length': '用户名长度不能大于16个字符',
        }
    )
    password = django_fields.RegexField(  # 判断密码，按照指定正则，长度为12-32位
        # ’^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@$\%\^\&\*\(\)])[0-9a-zA-Z!@#$\%\^\&\*\(\)]{8,32}$‘,
        '^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$\%\^\&\*\(\)])[0-9a-zA-Z!@#$\%\^\&\*\(\)]{8,32}$',
        min_length=8,
        max_length=32,
        error_messages={
            'required': '密码不能为空',
            'invalid': '密码必须包含数字、字母、特殊字符',
            'min_length': '密码长度不能小于8个字符',
            'max_length': '密码长度不能大于32个字符',
        }
    )

